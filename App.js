import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { UserProvider } from "./UserContext";

import SignUp from "./src/Screens/SignUp";
import SignIn from "./src/Screens/SignIn";
import Calculator from "./src/Screens/Calculator";
import History from "./src/Screens/History";
import Home from "./src/Screens/Home";
import SignOut from "./src/Screens/SignOut";
import { SafeAreaProvider } from "react-native-safe-area-context";

const Stack = createStackNavigator();

const App = () => {
  return (
    <SafeAreaProvider>
      {/* Wrap the app with the UserProvider for context */}
      <UserProvider>
        {/* Set up the navigation container */}
        <NavigationContainer>
          {/* Define the stack navigator with initial route name */}
          <Stack.Navigator initialRouteName="Home">
            {/* Home screen */}
            <Stack.Screen
              name="Home"
              component={Home}
              options={{ headerShown: false }} // Hide the header
            />

            {/* Sign In screen */}
            <Stack.Screen
              name="SignIn"
              component={SignIn}
              options={{ headerShown: false }} // Hide the header
            />

            {/* Sign Up screen */}
            <Stack.Screen
              name="SignUp"
              component={SignUp}
              options={{ headerShown: false }} // Hide the header
            />

            {/* Calculator screen */}
            <Stack.Screen
              name="Calculator"
              component={Calculator}
              options={{ headerShown: false }} // Hide the header
            />

            {/* History screen */}
            <Stack.Screen
              name="History"
              component={History}
              options={{ headerShown: false }} // Hide the header
            />

            {/* Sign Out screen */}
            <Stack.Screen
              name="SignOut"
              component={SignOut}
              options={{ headerShown: false }} // Hide the header
            />
          </Stack.Navigator>
        </NavigationContainer>
      </UserProvider>
    </SafeAreaProvider>
  );
};

export default App;
