import React, { useState } from "react";
import { View, StyleSheet, ScrollView, Pressable, Text } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import Icon from "react-native-vector-icons/FontAwesome";
import { useNavigation } from "@react-navigation/native";

const Body = ({ Body, Lable, Navigates }) => {
  const insets = useSafeAreaInsets(); // Accessing safe area insets for handling screen edges
  const navigation = useNavigation(); // Accessing navigation object

  return (
    <View
      style={[
        styles.container,
        {
          paddingTop: insets.top,
          paddingBottom: insets.bottom,
          paddingLeft: insets.left,
          paddingRight: insets.right,
        },
      ]}
    >
      <View style={styles.containerBody}>
        <View style={styles.navContainer}>
          <Pressable onPress={() => navigation.navigate(Navigates)}>
            <Icon name="arrow-left" size={30} color={"#5B5E67"} />
          </Pressable>
          <Text style={styles.lable}>{Lable}</Text>
        </View>
        {Body}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  navContainer: {
    marginTop: 10,
    marginHorizontal: 5,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  containerBody: {
    height: "95%",
    backgroundColor: "#292A2D",
    marginTop: "auto",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
  },
  lable: {
    fontSize: 25,
    fontWeight: "600",
    color: "#fff",
    width: "60%",
  },
});

export default Body;
