import React from "react";
import { View, Text } from "react-native";

const Calculation = ({ Value }) => {
  return (
    <>
      <View>
        {Value.map((calc, index) => (
          <View key={index}>
            <Text>{calc}</Text>
          </View>
        ))}
      </View>
    </>
  );
};

export default Calculation;
