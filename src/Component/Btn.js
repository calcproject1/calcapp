import * as React from "react";
import { View, Text, Pressable, StyleSheet, Dimensions } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { UserContext } from "../../UserContext";

const CalculatorButtons = ({ values, onButtonPress }) => {
  const { username, token, font } = React.useContext(UserContext);
  const { width } = Dimensions.get("window");

  const buttonWidth = (width - 50) / 4; // Calculate the width of each button based on the available width

  const isOperator = (button) => {
    return ["+", "-", "x", "÷", "="].includes(button); // Check if the button is an operator (+, -, x, ÷, =)
  };
  const isSpecialButton = (button) => {
    return ["AC", "+/-", "%"].includes(button); // Check if the button is a special button (AC, +/-, %)
  };

  const renderButtonContent = (button) => {
    if (button === "") {
      return <Icon name="history" style={styles.buttonText} />; // Render a history icon if the button is empty
    } else {
      return (
        <Text style={[styles.buttonText, { fontFamily: font }]}>{button}</Text> // Render the button text with the specified font
      );
    }
  };

  return (
    <View style={styles.container}>
      {values.map((button, index) => (
        <Pressable
          key={index}
          style={[
            styles.button,
            { width: buttonWidth, height: buttonWidth },
            isOperator(button) && styles.operatorButton,
            isSpecialButton(button) && styles.specialButton,
          ]}
          onPress={() => onButtonPress(button)}
        >
          {renderButtonContent(button)}
        </Pressable>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
  },
  button: {
    flex: 1,
    height: 80,
    justifyContent: "center",
    alignItems: "center",
    margin: 1,
    borderRadius: 15,
    backgroundColor: "#3B3D43",
    fontFamily: "Inter",
    fontWeight: "400",
  },
  operatorButton: {
    backgroundColor: "#3764B4",
  },
  specialButton: {
    backgroundColor: "#5B5E67",
  },
  buttonText: {
    fontSize: 44,
    color: "#fff",
  },
});

export default CalculatorButtons;
