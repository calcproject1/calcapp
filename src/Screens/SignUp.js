import React, { useState } from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  TextInput,
  Pressable,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import axios from "axios";
import Body from "../Component/Body";

const SignUp = () => {
  // State variables to store user input
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigation = useNavigation();

  // Function to handle sign up process
  const enter = async () => {
    try {
      const reqBody = {
        name: name,
        age: age,
        email: email,
        password: password,
      };

      // Send a POST request to the create endpoint
      const response = await axios.post(
        "https://calcapi.onrender.com/create/",
        reqBody,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const data = response.data;

      if (data.status) {
        // If sign up is successful, navigate to the SignIn screen
        navigation.navigate("SignIn");

        // Reset the input fields
        setName("");
        setAge("");
        setEmail("");
        setPassword("");
      } else {
        console.log(`${data.message}`);
      }
    } catch (error) {
      console.error("Error fetching user:", error);
    }
  };

  return (
    // Render the Body component with the provided content
    <Body
      Body={
        <View style={styles.container}>
          <View style={styles.formContainer}>
            {/* Input field for name */}
            <TextInput
              style={styles.input}
              value={name}
              placeholder={"name"}
              onChangeText={(text) => setName(text)}
            />

            {/* Input field for age */}
            <TextInput
              style={styles.input}
              value={age}
              placeholder={"age"}
              onChangeText={(text) => setAge(text)}
            />

            {/* Input field for email */}
            <TextInput
              style={styles.input}
              value={email}
              placeholder={"email"}
              onChangeText={(text) => setEmail(text)}
              autoCapitalize={"none"}
            />

            {/* Input field for password */}
            <TextInput
              style={styles.input}
              value={password}
              placeholder={"Password"}
              secureTextEntry
              onChangeText={(text) => setPassword(text)}
            />

            {/* Sign Up button */}
            <Button title={"Sign Up"} onPress={enter} />
          </View>

          <View style={styles.linkContainer}>
            <Text style={styles.text}>Already have an account?</Text>

            {/* Link to navigate to the SignIn screen */}
            <Pressable onPress={() => navigation.navigate("SignIn")}>
              <Text style={[styles.text, { color: "#3764B4" }]}> Sign In</Text>
            </Pressable>
          </View>
        </View>
      }
      Lable={"Sign Up"}
      Navigates={"SignIn"}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  formContainer: {
    width: "100%",
    paddingHorizontal: 20,
  },
  input: {
    height: 40,
    marginBottom: 15,
    backgroundColor: "#fff",
  },
  linkContainer: {
    flexDirection: "row",
    marginTop: 20,
  },
  text: {
    color: "#fff",
    fontSize: 20,
    marginEnd: 5,
    marginTop: 10,
  },
});

export default SignUp;
