import React, { useContext, useEffect } from "react";
import { useNavigation } from "@react-navigation/native";
import { TouchableOpacity, Text, StyleSheet, View } from "react-native";
import { UserContext } from "../../UserContext";
import Body from "../Component/Body";

const SignOut = () => {
  const { unsetUser, setUser } = useContext(UserContext); // Accessing user context to unset and set user data
  const navigation = useNavigation(); // Accessing navigation object

  useEffect(() => {
    const logout = async () => {
      await unsetUser(); // Unsetting the user data
      setUser({}); // Setting an empty user object
      navigation.navigate("Calculator"); // Navigating to the Calculator screen after logout
    };

    logout(); // Calling the logout function when the component mounts
  }, []);

  return (
    <Body
      Body={
        <View style={styles.container}>
          <TouchableOpacity onPress={() => navigation.navigate("Calculator")}>
            <Text style={styles.text}>Logging out...</Text>
          </TouchableOpacity>
        </View>
      }
      Lable={"SignOut"}
      Navigates={"Home"}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    color: "#fff",
    fontSize: 20,
  },
});

export default SignOut;
