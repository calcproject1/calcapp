import React, { useContext } from "react";
import { View, Text, Button, StyleSheet, Image, Pressable } from "react-native";
import { UserContext } from "../../UserContext";
import { useNavigation } from "@react-navigation/native";
import Body from "../Component/Body";

const Home = () => {
  // Get user data from the UserContext
  const { user, width, heigth } = useContext(UserContext);
  const imgWigth = width / 2;
  console.log(imgWigth);

  const navigation = useNavigation();

  // Function to navigate to the SignOut screen
  const logOut = () => {
    navigation.navigate("SignOut");
  };

  // Function to navigate to the Calculator screen
  const navigateToCalculator = () => {
    console.log("go to calcu");
    navigation.navigate("Calculator");
  };

  return (
    // Render the Body component with the provided content
    <Body
      Body={
        <View style={styles.container}>
          <View style={styles.Links}>
            {/* Navigate to Calculator screen */}
            <Pressable
              stlye={styles.button}
              onPress={() => navigation.navigate("Calculator")}
            >
              <Text style={styles.linkText}>Calculator</Text>
            </Pressable>
            {/* Navigate to History screen */}
            <Pressable
              stlye={styles.button}
              onPress={() => navigation.navigate("History")}
            >
              <Text style={styles.linkText}>History</Text>
            </Pressable>
            {!user.name ? (
              // If user is not logged in, navigate to SignIn screen
              <Pressable
                stlye={styles.button}
                onPress={() => navigation.navigate("SignIn")}
              >
                <Text style={styles.linkText}>Sign In</Text>
              </Pressable>
            ) : (
              // If user is logged in, show Sign Out button
              <Pressable stlye={styles.button} onPress={logOut}>
                <Text style={styles.linkText}>Sign Out</Text>
              </Pressable>
            )}
          </View>
          <View style={[styles.imgWrap]}>
            <Pressable onPress={navigateToCalculator}>
              <Image
                alt="Calculator"
                style={[styles.stretch, { width: imgWigth }]}
                source={{
                  uri: "https://upload.wikimedia.org/wikipedia/commons/5/55/Windows_Calculator_icon.png",
                }}
              />
            </Pressable>
          </View>
        </View>
      }
      Lable={!user.name ? "Home" : `${user.name}`}
      Navigates={"Home"}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
  },
  textLable: {
    fontSize: 23,
    color: "#fff",
  },
  text: {
    fontSize: 20,
    color: "#fff",
  },
  imgWrap: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  Links: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10,
  },
  linkText: {
    fontSize: 20,
    color: "#fff",
  },
});

export default Home;
