import * as React from "react";
import { View, Text, Button, StyleSheet, TextInput } from "react-native";
import { UserContext } from "../../UserContext";
import CalculatorButtons from "../Component/Btn";
import Body from "../Component/Body";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useNavigation } from "@react-navigation/native";

const Calculator = () => {
  const { user } = React.useContext(UserContext);
  const navigation = useNavigation();

  // State variables to store input value and calculation result
  const [inputValue, setInputValue] = React.useState("");
  const [inputResult, setInputResult] = React.useState("");

  // Function to handle input value change
  const handleInput = (text) => {
    setInputValue(text);
  };

  // Function to save the calculation to the server
  const saveCalculation = async ({ res, val }) => {
    try {
      const token = await AsyncStorage.getItem("token");
      const reqBody = {
        calcProblem: val,
        calcAnswer: res,
      };

      if (token) {
        // Send a POST request to the calculation endpoint with the calculation data
        const response = await axios.post(
          "https://calcapi.onrender.com/calculation/",
          reqBody,
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`,
            },
          }
        );
        const data = response.data;
        console.log(data);
      }
    } catch (error) {
      console.error("Error fetching user:", error);
    }
  };

  // Function to handle the equals button press
  const handleEquals = async () => {
    let result = "";

    try {
      if (inputValue == 0) {
        result = "0";
      } else {
        let value = eval(inputValue);

        if (value >= 1000000000) {
          let newValue =
            (value / 1000000000).toFixed(1).replace(/\.0$/, "") + "B";
          result = newValue.toString();
        } else if (value > 9999999) {
          let newValue = (value / 1000000).toFixed(1).replace(/\.0$/, "") + "M";
          result = newValue.toString();
        } else {
          result = value.toString();
        }
      }

      // Save the calculation
      saveCalculation({ res: result, val: inputValue });
    } catch (error) {
      result = "Error";
    }

    // Update the input result and clear the input value
    setInputResult(result);
    setInputValue("");
  };

  // Function to handle number button press
  const handleNumberPress = (num) => {
    let newInput = "";

    if (num === "+/-") {
      if (inputValue.startsWith("-")) {
        newInput = inputValue.substring(1);
      } else {
        newInput = `-${inputValue}`;
      }
    } else if (num === ".") {
      if (inputValue.length === 0) {
        newInput = inputValue + ".";
      } else if (inputValue.endsWith(".")) {
        newInput = inputValue;
      } else {
        newInput = inputValue + num.toString();
      }
    } else if (
      // Check for valid operations before appending the number
      (num === "-" &&
        (inputValue.endsWith("-") ||
          inputValue.endsWith("+") ||
          inputValue.endsWith("*") ||
          inputValue.endsWith("/"))) ||
      (num === "+" &&
        (inputValue.endsWith("-") ||
          inputValue.endsWith("+") ||
          inputValue.endsWith("*") ||
          inputValue.endsWith("/"))) ||
      (num === "*" &&
        (inputValue.endsWith("-") ||
          inputValue.endsWith("+") ||
          inputValue.endsWith("*") ||
          inputValue.endsWith("/"))) ||
      (num === "/" &&
        (inputValue.endsWith("-") ||
          inputValue.endsWith("+") ||
          inputValue.endsWith("*") ||
          inputValue.endsWith("/")))
    ) {
      newInput = inputValue;
    } else {
      newInput = inputValue + num.toString();
    }

    setInputValue(newInput);
  };

  // Function to handle button press
  const handleButtonPress = (button) => {
    if (button === "=") {
      handleEquals();
    } else if (button === "AC") {
      setInputValue("");
      setInputResult("");
    } else if (button === "+/-") {
      setInputValue((prevValue) =>
        prevValue.startsWith("-") ? prevValue.slice(1) : `-${prevValue}`
      );
    } else if (button === "x") {
      handleNumberPress("*");
    } else if (button === "÷") {
      handleNumberPress("/");
    } else if (button === "") {
      navigation.navigate("History");
    } else {
      handleNumberPress(button);
    }
  };

  return (
    <Body
      Body={
        <View style={styles.container}>
          <View style={styles.topContainer}>
            <TextInput
              onChangeText={handleInput}
              value={inputValue}
              placeholder="0"
              editable={false}
              placeholderTextColor="#5B5E67"
              style={[styles.input, styles.inputTop]}
            />
            <TextInput
              style={[styles.input, styles.inputBottom]}
              value={inputResult}
              editable={false}
            />
          </View>

          <View style={{ width: "100%", padding: 5 }}>
            {/* Calculator buttons component */}
            <CalculatorButtons
              values={["AC", "+/-", "%", "÷"]}
              onButtonPress={handleButtonPress}
            />
            <CalculatorButtons
              values={["7", "8", "9", "x"]}
              onButtonPress={handleButtonPress}
            />
            <CalculatorButtons
              values={["4", "5", "6", "-"]}
              onButtonPress={handleButtonPress}
            />
            <CalculatorButtons
              values={["1", "2", "3", "+"]}
              onButtonPress={handleButtonPress}
            />
            <CalculatorButtons
              values={[".", "0", "", "="]}
              onButtonPress={handleButtonPress}
            />
          </View>
        </View>
      }
      Lable={!user.name ? "Caculator" : `${user.name}`}
      Navigates={"Home"}
    />
  );
};

const styles = StyleSheet.create({
  topContainer: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-end",
  },
  container: {
    flex: 1,
  },
  input: {
    textAlign: "right",
    marginTop: 20,
  },
  inputTop: {
    fontSize: 35,
    color: "#5B5E67",
  },
  inputBottom: {
    fontSize: 89,
    marginBottom: 20,
    color: "#fff",
  },
});

export default Calculator;
