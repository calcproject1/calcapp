import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, Pressable, Modal } from "react-native";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Body from "../Component/Body";
import Icon from "react-native-vector-icons/FontAwesome";

const History = () => {
  const [calc, setCalc] = useState([]); // State variable to store calculation history
  const [modalVisible, setModalVisible] = useState(false); // State variable for modal visibility

  // Function to fetch calculation history from the server
  const fetchHistory = async () => {
    try {
      const token = await AsyncStorage.getItem("token");
      if (token) {
        const response = await axios.get(
          "https://calcapi.onrender.com/all/history/",
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        const data = response.data;
        if (data.status) {
          setCalc(data.calcHistory);
        } else {
          setCalc([]);
        }
      } else {
        setCalc([]);
      }
    } catch (error) {
      console.error("Error fetching history:", error);
    }
  };

  // Function to clear calculation history
  const clearHistory = async () => {
    try {
      const token = await AsyncStorage.getItem("token");

      if (token) {
        const response = await axios.put(
          "https://calcapi.onrender.com/clear-history/",
          {},
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json",
            },
          }
        );
        const data = response.data;
        if (data.status) {
          setModalVisible(!modalVisible);
          fetchHistory();
        } else {
          setModalVisible(true);
        }
      } else {
        setModalVisible(true);
      }
    } catch (error) {
      console.error("Error fetching history:", error);
    }
  };

  useEffect(() => {
    fetchHistory();
  }, []);

  return (
    <Body
      Body={
        <View>
          <View style={styles.centeredView}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                setModalVisible(!modalVisible);
              }}
            >
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  {!calc.length ? (
                    <Text style={styles.modalText}>History Empty!</Text>
                  ) : (
                    <Text style={styles.modalText}>Clear History!</Text>
                  )}
                  {!calc.length ? (
                    <View style={styles.buttonsWrap}>
                      <Pressable
                        style={[styles.button, styles.buttonClose]}
                        onPress={() => setModalVisible(!modalVisible)}
                      >
                        <Text style={styles.textStyle}>Back</Text>
                      </Pressable>
                    </View>
                  ) : (
                    <View style={styles.buttonsWrap}>
                      <Pressable
                        style={[styles.button, styles.buttonClose]}
                        onPress={() => setModalVisible(!modalVisible)}
                      >
                        <Text style={styles.textStyle}>Cancel</Text>
                      </Pressable>

                      <Pressable
                        style={[styles.button, styles.buttonClose]}
                        onPress={clearHistory}
                      >
                        <Text style={styles.textStyle}>Confirm</Text>
                      </Pressable>
                    </View>
                  )}
                </View>
              </View>
            </Modal>
          </View>
          <Pressable
            style={styles.container}
            onPress={() => setModalVisible(true)}
          >
            <Icon name="trash" style={styles.navLable} />
          </Pressable>
          {!calc.length ? (
            <View style={styles.emptyHistory}>
              <Text style={styles.textCalc}>Empty!</Text>
              <Text style={styles.textCalc}>Do Some Calculation</Text>
            </View>
          ) : (
            <View>
              {calc.map((item, index) => (
                <View
                  style={[
                    styles.historyBody,
                    index === 0 && {
                      borderTopStartRadius: 10,
                      borderTopEndRadius: 10,
                    },
                    index === calc.length - 1 && {
                      borderBottomEndRadius: 10,
                      borderBottomStartRadius: 10,
                    },
                  ]}
                  key={item._id}
                >
                  <Text style={styles.textCalc}>{item.calcProblem}</Text>
                  <Text style={styles.textCalc}>= {item.calcAnswer}</Text>
                </View>
              ))}
            </View>
          )}
        </View>
      }
      Lable={"History"}
      Navigates={"Calculator"}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
    marginHorizontal: 10,
  },
  emptyHistory: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: 15,
    marginHorizontal: 10,
    backgroundColor: "#4D5057",
    marginTop: 2,
    borderRadius: 10,
  },
  historyBody: {
    padding: 15,
    marginHorizontal: 10,
    backgroundColor: "#4D5057",
    marginTop: 2,
  },
  navLable: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "right",
  },
  textCalc: {
    color: "#fff",
    fontSize: 23,
    fontWeight: "600",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  buttonsWrap: {
    flexDirection: "row",
  },
  button: {
    borderRadius: 5,
    marginHorizontal: 10,
    padding: 5,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});

export default History;
