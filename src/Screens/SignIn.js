import React, { useState, useContext, useEffect } from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  TextInput,
  Pressable,
} from "react-native";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Body from "../Component/Body";
import { useNavigation } from "@react-navigation/native";
import { UserContext } from "../../UserContext";

const SignIn = () => {
  const { user, setUser } = useContext(UserContext);

  // State variables to store user input
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigation = useNavigation();

  // Function to handle sign in process
  const enter = () => {
    const requestBody = {
      email: email,
      password: password,
    };

    // Send a POST request to the login endpoint
    axios
      .post("https://calcapi.onrender.com/login/", requestBody)
      .then((response) => {
        console.log("Response:", response.data.user);

        // Set user data in the UserContext
        setUser({
          id: response.data.user._id,
          email: response.data.user.email,
          name: response.data.user.name,
        });

        console.log(user);

        // Navigate to the Calculator screen
        navigation.navigate("Calculator");

        // Store the token in AsyncStorage
        AsyncStorage.setItem("token", response.data.token)
          .then(() => {
            console.log("Token stored successfully!");
          })
          .catch((error) => {
            console.error("Error:", error);
          });
      });
  };

  // Check if the user is already signed in and navigate to Calculator screen
  useEffect(() => {
    if (user.id) {
      navigation.navigate("Calculator");
    }
  }, [user, navigation]);

  return (
    // Render the Body component with the provided content
    <Body
      Body={
        <View style={styles.container}>
          <View style={styles.formContainer}>
            {/* Input field for email */}
            <TextInput
              style={styles.input}
              value={email}
              placeholder={"email"}
              onChangeText={(text) => setEmail(text)}
              autoCapitalize={"none"}
            />

            {/* Input field for password */}
            <TextInput
              style={styles.input}
              value={password}
              placeholder={"Password"}
              secureTextEntry
              onChangeText={(text) => setPassword(text)}
            />

            {/* Sign In button */}
            <Button title={"Sign In"} onPress={enter} />
          </View>

          <View style={styles.linkContainer}>
            <Text style={styles.text}>Don't have an account?</Text>

            {/* Link to navigate to the SignUp screen */}
            <Pressable onPress={() => navigation.navigate("SignUp")}>
              <Text style={[styles.text, { color: "#3764B4" }]}> Sign Up</Text>
            </Pressable>
          </View>
        </View>
      }
      Lable={"SignIn"}
      Navigates={"Home"}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  formContainer: {
    width: "100%",
    paddingHorizontal: 20,
  },
  input: {
    height: 40,
    marginBottom: 15,
    backgroundColor: "#fff",
  },
  linkContainer: {
    flexDirection: "row",
    marginTop: 20,
  },
  text: {
    color: "#fff",
    fontSize: 20,
    marginEnd: 5,
    marginTop: 10,
  },
});

export default SignIn;
