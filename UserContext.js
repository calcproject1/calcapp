import React, { createContext, useState, useEffect } from "react";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Dimensions } from "react-native";

// Create the user context
export const UserContext = createContext();

export const UserProvider = ({ children }) => {
  // Get the device dimensions
  const { width, height } = Dimensions.get("window");

  // Set up the user state
  const [user, setUser] = useState({
    id: null,
    email: null,
    name: null,
  });

  useEffect(() => {
    // Fetch the user data
    const fetchUser = async () => {
      try {
        // Get the token from AsyncStorage
        const token = await AsyncStorage.getItem("token");

        // Reset user data
        const resetUser = {
          id: null,
          email: null,
          name: null,
        };

        if (token) {
          // If token exists, make a request to get user details
          const response = await axios.get(
            "https://calcapi.onrender.com/details/",
            {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }
          );
          const data = response.data;

          if (data.status) {
            // Update user data if the request was successful
            setUser((prevUser) => ({
              ...prevUser,
              id: data.user._id,
              email: data.user.email,
              name: data.user.name,
            }));
            console.log("UserContext", user);
          } else {
            // Reset user data if the request failed
            setUser(resetUser);
          }
        } else {
          // Reset user data if no token exists
          setUser(resetUser);
        }
      } catch (error) {
        // Handle errors and reset user data
        setUser({
          id: null,
          email: null,
          name: null,
        });
        console.error("Error fetching user:", error);
      }
    };

    fetchUser();
  }, []);

  // Function to clear user data from AsyncStorage
  const unsetUser = async () => {
    try {
      await AsyncStorage.clear();
    } catch (error) {
      console.error("Error clearing AsyncStorage:", error);
    }
    setUser({
      id: null,
      email: null,
      name: null,
    });
  };

  return (
    // Provide the user context values to the children components
    <UserContext.Provider value={{ user, setUser, unsetUser, width, height }}>
      {children}
    </UserContext.Provider>
  );
};
